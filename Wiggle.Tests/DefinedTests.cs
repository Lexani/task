﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wiggle.Model;
using Wiggle.Model.ConstantReferences;

namespace Wiggle.Tests
{
    [TestClass]
    public class DefinedTests
    {
        [TestMethod]
        public void Basket1()
        {
            var customer = new Customer();
            var hat = new Product("Hat", new decimal(10.5), ProductType.HeadGear);
            customer.Basket.AddProduct(hat);
            var jumper = new Product("Jumper", new decimal(54.65), ProductType.Unknown);
            customer.Basket.AddProduct(jumper);

            var voucher = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);

            Assert.AreEqual(VoucherState.Appliable, voucher.State);
            Assert.AreEqual(new decimal(60.15), customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void Basket2()
        {
            var customer = new Customer();
            var hat = new Product("Hat", 25, ProductType.Unknown);
            customer.Basket.AddProduct(hat);
            var jumper = new Product("Jumper", 26, ProductType.Unknown);
            customer.Basket.AddProduct(jumper);

            var voucher = new OfferVoucher(50, x => 5, new [] {ProductType.HeadGear});
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);

            Assert.AreEqual(VoucherState.NoAppliableProducts, voucher.State);
            Assert.AreEqual(51, customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void Basket3()
        {
            var customer = new Customer();
            var hat = new Product("Hat", 25, ProductType.Unknown);
            customer.Basket.AddProduct(hat);
            var jumper = new Product("Jumper", 26, ProductType.Unknown);
            customer.Basket.AddProduct(jumper);
            var headLight = new Product("HeadLight", new decimal(3.50), ProductType.HeadGear);
            customer.Basket.AddProduct(headLight);

            var voucher = new OfferVoucher(50, x => 5, new[] { ProductType.HeadGear });
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);

            Assert.AreEqual(VoucherState.NotReachThreshold, voucher.State);
            Assert.AreEqual(new decimal(54.5), customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void Basket4()
        {
            var customer = new Customer();
            var hat = new Product("Hat", 25, ProductType.Unknown);
            customer.Basket.AddProduct(hat);
            var jumper = new Product("Jumper", 26, ProductType.Unknown);
            customer.Basket.AddProduct(jumper);

            var giftVoucher = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(giftVoucher);
            customer.Basket.AddVoucher(giftVoucher);
            var offerVoucher = new OfferVoucher(50, x => 5);
            customer.PersonalVouchers.Add(offerVoucher);
            customer.Basket.AddVoucher(offerVoucher);

            Assert.AreEqual(VoucherState.Appliable, offerVoucher.State);
            Assert.AreEqual(VoucherState.Appliable, giftVoucher.State);
            Assert.AreEqual(41, customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void Basket5()
        {
            var customer = new Customer();
            var hat = new Product("Hat", 25, ProductType.Unknown);
            customer.Basket.AddProduct(hat);
            var jumper = new Product("GiftVoucher", 30, ProductType.GiftVoucher);
            customer.Basket.AddProduct(jumper);

            var voucher = new OfferVoucher(50, x => 5);
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);

            Assert.AreEqual(VoucherState.NotReachThreshold, voucher.State);
            Assert.AreEqual(55, customer.Basket.OverallPrice);
        }
    }
}
