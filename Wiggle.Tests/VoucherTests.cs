﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wiggle.Model;
using Wiggle.Model.ConstantReferences;

namespace Wiggle.Tests
{
    [TestClass]
    public class VoucherTests
    {
        [TestMethod]
        public void ShouldHaveNotAppliedStateIfRemovedFromBasket()
        {
            var customer = new Customer();
            var voucher = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);
            var result = customer.Basket.RemoveVoucher(voucher);

            Assert.IsTrue(result);
            Assert.AreEqual(VoucherState.NotApplied, voucher.State);
            Assert.IsNull(voucher.AffectedBasket);
        }


        [TestMethod]
        public void GiftVousherShouldHaveAppliedStateIfAppliedToBasketWithProducts()
        {
            var customer = new Customer();
            var p = new Product("Hat", 25, ProductType.HeadGear);
            var result = customer.Basket.AddProduct(p);
            Assert.IsTrue(result);

            var voucher = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);
            Assert.AreEqual(VoucherState.Appliable, voucher.State);
        }

        [TestMethod]
        public void ShouldNotAppliedToGiftVousherProducts()
        {
            var customer = new Customer();
            var p = new Product("GiftVousher", 25, ProductType.GiftVoucher);
            var result = customer.Basket.AddProduct(p);
            Assert.IsTrue(result);

            var voucher = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);
            var offer = new OfferVoucher(10, x => 5);
            customer.PersonalVouchers.Add(offer);
            customer.Basket.AddVoucher(offer);

            Assert.AreEqual(VoucherState.NoAppliableProducts, voucher.State);
            Assert.AreEqual(VoucherState.NoAppliableProducts, offer.State);
            Assert.AreEqual(25, customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void OfferVoucherShouldGetDiscountToSubsetOfProducts()
        {
            var customer = new Customer();
            var p = new Product("GiftVousher", 10, ProductType.GiftVoucher);
            customer.Basket.AddProduct(p);
            var hat = new Product("Hat", 10, ProductType.HeadGear);
            customer.Basket.AddProduct(hat);

            var voucher = new OfferVoucher(10, x => 5, new [] {ProductType.GiftVoucher });
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);

            Assert.AreEqual(VoucherState.Appliable, voucher.State);
            Assert.AreEqual(15, customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void OfferVoucherShouldNotGetDiscountToSubsetOfProducts()
        {
            var customer = new Customer();
            var p = new Product("GiftVousher", 10, ProductType.GiftVoucher);
            customer.Basket.AddProduct(p);
            var hat = new Product("Hat", 10, ProductType.HeadGear);
            customer.Basket.AddProduct(hat);

            var voucher = new OfferVoucher(15, x => 5, new[] { ProductType.GiftVoucher });
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);

            Assert.AreEqual(VoucherState.NotReachThreshold, voucher.State);
            Assert.AreEqual(20, customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void OfferVoucherShouldHaveNoProductsState()
        {
            var customer = new Customer();
            var p = new Product("GiftVousher", 10, ProductType.GiftVoucher);
            customer.Basket.AddProduct(p);
            var hat = new Product("Hat", 10, ProductType.HeadGear);
            customer.Basket.AddProduct(hat);

            var voucher = new OfferVoucher(10, x => 5, new[] { ProductType.Unknown });
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);

            Assert.AreEqual(VoucherState.NoAppliableProducts, voucher.State);
            Assert.AreEqual(20, customer.Basket.OverallPrice);
        }
    }
}
