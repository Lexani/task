﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wiggle.Model;
using Wiggle.Model.ConstantReferences;

namespace Wiggle.Tests
{
    [TestClass]
    public class BasketTests
    {
        [TestMethod]
        public void ShouldAddVousherToTheBasket()
        {
            var customer = new Customer();
            var voucher = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher);
            var addResult = customer.Basket.AddVoucher(voucher);

            Assert.IsTrue(addResult);
            Assert.IsNotNull(voucher.AffectedBasket);
            Assert.AreNotEqual(VoucherState.NotApplied, voucher.State);
        }

        [TestMethod]
        public void ShouldAddManyGiftVoushersToTheBasket()
        {
            var customer = new Customer();
            var voucher1 = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher1);
            var result1 = customer.Basket.AddVoucher(voucher1);
            var voucher2 = new GiftVoucher(x => 2);
            customer.PersonalVouchers.Add(voucher2);
            var result2 = customer.Basket.AddVoucher(voucher2);

            Assert.IsTrue(result1);
            Assert.IsTrue(result2);
        }


        [TestMethod]
        public void ShouldApplyVouchersInRightOrder()
        {
            var customer = new Customer();
            var p = new Product("Hat", 15, ProductType.HeadGear);
            customer.Basket.AddProduct(p);

            var voucher1 = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher1);
            customer.Basket.AddVoucher(voucher1);
            var voucher2 = new OfferVoucher(15, x => 5);
            customer.PersonalVouchers.Add(voucher2);
            customer.Basket.AddVoucher(voucher2);

            Assert.AreEqual(VoucherState.Appliable, voucher1.State);
            Assert.AreEqual(VoucherState.Appliable, voucher2.State);
            Assert.AreEqual(5, customer.Basket.OverallPrice);
        }


        [TestMethod]
        public void ShouldAddOnlyOneOfferVousher()
        {
            var customer = new Customer();
            var voucher1 = new OfferVoucher(10, x => 5);
            customer.PersonalVouchers.Add(voucher1);
            var firstAddition = customer.Basket.AddVoucher(voucher1);
            var voucher2 = new OfferVoucher(15, x => 5);
            customer.PersonalVouchers.Add(voucher2);
            var secondRAddition = customer.Basket.AddVoucher(voucher2);

            Assert.IsTrue(firstAddition);
            Assert.IsFalse(secondRAddition);
        }

        [TestMethod]
        public void ShouldAddProductSnapshots()
        {
            var customer = new Customer();
            var p = new Product("Hat", 25, ProductType.HeadGear);
            var quantity = 2;
            var result = customer.Basket.AddProduct(p, quantity);

            Assert.IsTrue(result);
            Assert.AreEqual(customer.Basket.ProductSnapshots[0].Product, p);
            Assert.AreEqual(quantity, customer.Basket.ProductSnapshots[0].Quantity);
        }

        [TestMethod]
        public void ShouldNotAddVousherIfNotInCustomerVoushers()
        {
            var vousher = new GiftVoucher(x => 5);
            var customer = new Customer();
            Assert.IsFalse(customer.Basket.AddVoucher(vousher));
        }

        [TestMethod]
        public void ShouldReturnZeroIfVoucherDiscountIsBiggerThanOverallPrice()
        {
            var customer = new Customer();
            var voucher = new GiftVoucher(x => 5);
            var product = new Product("Hat", 2, ProductType.HeadGear);
            customer.Basket.AddProduct(product);
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);

            Assert.AreEqual(decimal.Zero, customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void ShouldReturnDiscountPriceWithGiftVousher()
        {
            var customer = new Customer();
            var p = new Product("Hat", 25, ProductType.HeadGear);
            var result = customer.Basket.AddProduct(p);
            Assert.IsTrue(result);

            var voucher = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);
            Assert.AreEqual(20, customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void ShouldReturnDiscountPriceWithOfferVousher()
        {
            var customer = new Customer();
            var p = new Product("Hat", 10, ProductType.HeadGear);
            var result = customer.Basket.AddProduct(p);
            Assert.IsTrue(result);

            var voucher = new OfferVoucher(10, x => 5);
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);
            Assert.AreEqual(5, customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void ShouldNotReturnDiscountPriceWithOfferVousher()
        {
            var customer = new Customer();
            var p = new Product("Hat", 10, ProductType.HeadGear);
            var result = customer.Basket.AddProduct(p);
            Assert.IsTrue(result);

            var voucher = new OfferVoucher(15, x => 5);
            customer.PersonalVouchers.Add(voucher);
            customer.Basket.AddVoucher(voucher);
            Assert.AreEqual(10, customer.Basket.OverallPrice);
        }

        [TestMethod]
        public void ShouldNotAddVousherIfHasBeenAlreadyApplied()
        {
            var customer = new Customer();
            var voucher = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher);
            var initAddResult = customer.Basket.AddVoucher(voucher);
            var secondAddResult = customer.Basket.AddVoucher(voucher);

            Assert.IsTrue(initAddResult);
            Assert.IsFalse(secondAddResult);
        }

        [TestMethod]
        public void ShouldRemoveVousherToTheBasket()
        {
            var customer = new Customer();
            var voucher = new GiftVoucher(x => 5);
            customer.PersonalVouchers.Add(voucher);
            var addResult = customer.Basket.AddVoucher(voucher);
            var removeResult = customer.Basket.RemoveVoucher(voucher);

            Assert.IsTrue(addResult);
            Assert.IsTrue(removeResult);
            Assert.IsNull(voucher.AffectedBasket);
            Assert.AreEqual(VoucherState.NotApplied, voucher.State);
        }
    }
}
