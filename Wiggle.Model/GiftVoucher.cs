﻿using System;
using System.Linq;
using Wiggle.Model.ConstantReferences;

namespace Wiggle.Model
{
    public class GiftVoucher : AbstractVoucher
    {
        public GiftVoucher(Func<Basket, decimal> discountFunc) : base(discountFunc)
        {
            this.Type = VoucherType.Gift;
            this.Priority = VoucherPriority.GiftVousher;
        }

        public override string ToString()
        {
            return string.Concat("Gift voucher", Name);
        }

        protected override VoucherState GetVoucherState()
        {
            return AffectedBasket.ProductSnapshots.Any(it => SuitableProductTypes.Contains(it.Product.Type))
                ? VoucherState.Appliable
                : VoucherState.NoAppliableProducts;
        }

    }
}
