﻿using Wiggle.Model.ConstantReferences;

namespace Wiggle.Model
{
    public class Product : Entity
    {

        public Product(string name, decimal price, ProductType type)
        {
            this.Name = name;
            this.Price = price;
            this.Type = type;
        }
        public decimal Price { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ProductType Type { get; set; }
    }
}
