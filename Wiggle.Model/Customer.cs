﻿using System.Collections.Generic;

namespace Wiggle.Model
{
    public class Customer : Entity
    {
        public Customer()
        {
            PersonalVouchers = new List<AbstractVoucher>();
            Basket = new Basket(this);
        }
        public IList<AbstractVoucher> PersonalVouchers { get; set; }

        public Basket Basket { get; set; }

        public string Name { get; set; }

    }
}
