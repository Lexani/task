﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wiggle.Model.ConstantReferences;

namespace Wiggle.Model
{
    public abstract class AbstractVoucher : Entity
    {
        protected AbstractVoucher(Func<Basket, decimal> discountFunc)
        {
            SuitableProductTypes = this.SuitableProductTypes = Enum.GetValues(typeof(ProductType))
                .Cast<ProductType>()
                .Except(new[] {ProductType.GiftVoucher})
                .ToList();
            this.DiscountFunc = discountFunc;
            this.State = VoucherState.NotApplied;
        }

        public string Name { get; set; }

        public VoucherType Type { get; set; }

        public VoucherState State { get; set; }

        public string Description { get; set; }

        public virtual bool CanApplyToBasket(Basket basket)
        {
            return State == VoucherState.NotApplied;
        }
        protected abstract VoucherState GetVoucherState();

        public VoucherState AddToBasket(Basket basket)
        {
            this.AffectedBasket = basket;
            UpdateVousherState();
            return State;
        }

        public void UpdateVousherState()
        {
            if (AffectedBasket == null)
            {
                State = VoucherState.NotApplied;
                return;
            }
            State = GetVoucherState();
        }

        public Basket AffectedBasket { get; set; }

        public virtual decimal GetDiscount()
        {
            UpdateVousherState();
            if (State != VoucherState.Appliable)
            {
                return default(decimal);
            }
            return DiscountFunc(AffectedBasket);
        }

        public override string ToString()
        {
            return Name;
        }

        public Func<Basket, decimal> DiscountFunc { get; private set; }

        public VoucherPriority Priority { get; protected set; }

        public virtual void CancelApplying()
        {
            this.AffectedBasket = null;
            this.State = VoucherState.NotApplied;
        }

        protected IList<ProductType> SuitableProductTypes { get; set; }

    }
}
