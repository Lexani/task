﻿namespace Wiggle.Model
{
    public class ProductSnapshot
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }

        public decimal OverallPrice => Product.Price * Quantity;
    }
}
