﻿namespace Wiggle.Model.ConstantReferences
{
    public enum ProductType
    {
        Unknown, 
        GiftVoucher, 
        HeadGear,
    }
}
