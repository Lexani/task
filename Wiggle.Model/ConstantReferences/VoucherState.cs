﻿namespace Wiggle.Model.ConstantReferences
{
    public enum VoucherState
    {
        Appliable,
        NotApplied,
        NoAppliableProducts,
        NotReachThreshold,
    }
}
