﻿namespace Wiggle.Model.ConstantReferences
{
    public enum VoucherPriority
    {
        OfferVousher = 1,
        GiftVousher = 2,
    }
}
