﻿using System;

namespace Wiggle.Model
{
    public class Entity
    {
        public Entity()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        /// <summary>
        ///   Overrides base equals.
        /// </summary>
        /// <permission cref = "System.Security.PermissionSet">public</permission>
        /// <param name = "obj">Object to compare with.</param>
        /// <returns>Returns the result indicating whether objects are equal.</returns>
        public override bool Equals(object obj)
        {
            Entity entity = obj as Entity;
            if (obj == null || entity == null || GetType() != entity.GetType())
            {
                return false;
            }
            return entity.Id.Equals(Id);
        }

        private int? hashCode;

        /// <summary>
        ///   Calculates hash code of the object.
        /// </summary>
        /// <permission cref = "System.Security.PermissionSet">public</permission>
        /// <returns>Returns hash code of the object.</returns>
        public override int GetHashCode()
        {
            if (hashCode == null)
            {
                hashCode = Id.GetHashCode();
            }
            return hashCode.Value;
        }
    }
}
