﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wiggle.Model.ConstantReferences;

namespace Wiggle.Model
{
    public class OfferVoucher : AbstractVoucher
    {

        public OfferVoucher(decimal threshold, Func<Basket, decimal> discountFunc,
            IEnumerable<ProductType> suitableProductTypes) : this(threshold, discountFunc)
        {
            this.SuitableProductTypes = suitableProductTypes.ToList();
        }

        public OfferVoucher(decimal threshold, Func<Basket, decimal> discountFunc) : base(discountFunc)
        {
            this.Type = VoucherType.Offer;
            this.threshold = threshold;
            this.Priority = VoucherPriority.OfferVousher;
        }

        public override bool CanApplyToBasket(Basket basket)
        {
            return base.CanApplyToBasket(basket) && basket.Vouchers.All(it => it.Type != this.Type);
        }

        public override string ToString()
        {
            return string.Concat("Offer voucher:", Name);
        }

        protected override VoucherState GetVoucherState()
        {
            var suitableProducts = AffectedBasket.ProductSnapshots
                .Where(it => SuitableProductTypes.Contains(it.Product.Type)).ToList();
            if (!suitableProducts.Any())
            {
                return VoucherState.NoAppliableProducts;
            }
            return suitableProducts.Sum(it => it.OverallPrice) >= threshold
                ? VoucherState.Appliable
                : VoucherState.NotReachThreshold;
        }

        private decimal threshold;
    }
}
