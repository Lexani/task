﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wiggle.Model
{
    public class Basket : Entity
    {
        public Basket(Customer customer)
        {
            ProductSnapshots = new List<ProductSnapshot>();
            Vouchers = new List<AbstractVoucher>();
            Customer = customer;
        }

        public IList<ProductSnapshot> ProductSnapshots { get; private set; }

        public IList<AbstractVoucher> Vouchers { get; private set; }

        public Customer Customer { get; set; }

        public string Name { get; set; }

        public decimal OverallPrice
        {
            get
            {
                var sum = ProductSnapshots.Sum(it => it.OverallPrice)
                    - Vouchers.OrderBy(it => it.Priority).Sum(it => it.GetDiscount());
                return sum > decimal.Zero ? sum : decimal.Zero;
            }
        }

        public bool AddVoucher(AbstractVoucher voucher)
        {
            if (Vouchers.Contains(voucher)
                || !voucher.CanApplyToBasket(this)
                || !Customer.PersonalVouchers.Contains(voucher))
            {
                return false;
            }
            voucher.AddToBasket(this);
            Vouchers.Add(voucher);
            return true;
        }

        public bool RemoveVoucher(AbstractVoucher voucher)
        {
            voucher.CancelApplying();
            return Vouchers.Remove(voucher);
        }

        public bool AddProduct(Product product, int quantity = 1)
        {
            if (ProductSnapshots.Any(it => it.Product.Id == product.Id))
            {
                return false;
            }
            var productSnapshot = new ProductSnapshot()
            {
                Product = product,
                Quantity = quantity
            };
            ProductSnapshots.Add(productSnapshot);
            UpdateVousherStates();
            return true;
        }

        public bool RemoveProduct(Product product)
        {
            var productSnapshot = ProductSnapshots.FirstOrDefault(it => it.Product == product);
            var result = productSnapshot != null && ProductSnapshots.Remove(productSnapshot);
            if (result)
            {
                UpdateVousherStates();
            }
            return result;
        }

        public override string ToString()
        {
            return string.Format("Basket #{0}, Name: {1}", Id, Name);
        }

        private void UpdateVousherStates()
        {
            foreach (var voucher in Vouchers)
            {
                voucher.UpdateVousherState();
            }
        }
    }
}
